package _Enseignant_.AT04;

import static org.junit.Assert.*;

import org.junit.Test;

public class AT04E01BaseTest {
	
	@Test
	public void testCalculeMoyenne() {
		//on ne fait qu'afficher le resultat 
		//de la moyenne ici avec un Epsilon
		
		
		int [] tab = {25, 12, 49, 10};
		assertEquals(24.00, AT04E01Base.calculeMoyenne(tab), 0.01);
		
		int [] tab1 = {35, 28, 40, 21};
		assertEquals(31.00, AT04E01Base.calculeMoyenne(tab1), 0.01);
		
		int [] tab2 = {0, 10, 10, -20};
		assertEquals(0.00, AT04E01Base.calculeMoyenne(tab2), 0.01);
	}
	
	@Test
	public void testTrouvePosMax() {
		//on affiche la position de la donnee maximale
		
		int[] tab = {4, 5, 7, 15, 2, 9};
		assertEquals(3, AT04E01Base.trouvePosMax(tab));
		
		int[] tab1 = {2, 14, 20, 2, 7, 9};
		assertEquals(2, AT04E01Base.trouvePosMax(tab1));
		
		int[] tab2 = {10, 13, 6, 8, 7, 5};
		assertEquals(1, AT04E01Base.trouvePosMax(tab2));
		
		
		
	}
	
	@Test
	public void testMatriceReguliere() {
		
		int[][] tab = {{10, 13, 6}, {8, 7, 5}};
		assertEquals(true, AT04E01Base.matriceReguliere(tab));
		
		int[][] tab1 = {{10, 13, 6, 5}, {8, 7, 5}};
		assertEquals(false, AT04E01Base.matriceReguliere(tab1));
		
		int[][] tab2 = null;
		assertEquals(false, AT04E01Base.matriceReguliere(tab2));
	}
	
	@Test
	public void testCalculerMoyenneJour() {
		//on affiche le resultat de la moyenne grace 
		//au jour preciser dans la matrice des donnees (case 8)
		
		
		int[][] tab = {{5, 4, 9}, {4, 8, 3}};
		assertEquals(5.000f, AT04E01Base.calculerMoyenneJour(tab, 2), 0.001);
		
		int[][] tab1 = null;
		assertEquals(Float.NaN, AT04E01Base.calculerMoyenneJour(tab1, 1), 0.001);
		
		int[][] tab2 = {{2, 4, 5, 6}};
		assertEquals(Float.NaN, AT04E01Base.calculerMoyenneJour(tab2, 2), 0.001);
		
	}

}
